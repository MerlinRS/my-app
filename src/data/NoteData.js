const NoteData = [
    {
      id: 1,
      text: 'This is my first note.',
    },
    {
      id: 2,
      text: 'This is my second note.',
    },
    {
      id: 3,
      text: 'This is my third note.',
    },
  ]
  
  export default NoteData;