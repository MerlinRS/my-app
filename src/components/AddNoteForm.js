import Header from "./Header";
import 'bootstrap/dist/css/bootstrap.css';
import {Form,Button} from 'react-bootstrap';
import { useEffect, useState,useContext } from "react";
import { NoteContext } from "../pages/Home";

function AddNoteForm({handleAdd,editText}) {
    const [text, setText] = useState()
    const {edit,handleUpdate} = useContext(NoteContext)

    const handleTextChange = (e) => {
        setText(e.target.value)
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        const newNotes = {
            text: text
        }
        if(edit.isEdit == false){
            // if(text.trim.length>10){
                handleAdd(newNotes)
                setText('')
            // }
        }
        else{
            handleUpdate(edit.id,newNotes)
            setText('')
        }

    }

    useEffect(() => { setText(editText) },[editText])

    return ( 
        <>
            <div className="noteFormContainer">
                <div className="noteForm">
                    <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label className="noteText">Add Note</Form.Label>
                            <Form.Control as="textarea" rows={3} placeholder="Type your note here..." onChange={handleTextChange} value={text}/>
                        </Form.Group>
                        <div className="AddNoteBtn">
                            <Button variant="primary" type="submit">
                                Add Note
                            </Button>
                        </div>
                    </Form>
                </div>
            </div>
        </>
    );
}

export default AddNoteForm;