import NoteList from './NoteList';
import 'bootstrap/dist/css/bootstrap.css';

function Note({notes,handleDelete,handleEdit}) {   

    return ( 
        <div className="note">
            <div className="noteHeader">
                <div className='row'>
                    <div className='col-md-10'>
                        Notes
                    </div>
                    <div className='col-md-2 noteCount'>
                        {notes.length} Notes
                    </div>
                </div>
            </div>
            <NoteList note={notes} handleDelete={handleDelete} handleEdit={handleEdit}/>
        </div>
    );
}

export default Note;