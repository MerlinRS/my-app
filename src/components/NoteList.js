import {FaTimes,FaEdit} from 'react-icons/fa'
import Card from "./Card";
import 'bootstrap/dist/css/bootstrap.css';

function NoteList({note,handleDelete,handleEdit}) {
    return ( 
        <div>
           {note.map((item) => (
                <Card key={item.id} item={item.text}>
                    <div className="row">
                        <div className="col-md-10">
                            {item.text}
                        </div>
                        <div className="col-md-1">
                            <button className="close" onClick={() => handleEdit(item.id,item.text)}><FaEdit color="red"/></button>
                        </div>
                        <div className="col-md-1">
                            <button className="close" onClick={() => handleDelete(item.id)}><FaTimes color="red"/></button>
                        </div>
                    </div>
                </Card>
            ))}
        </div>
     );
}

export default NoteList;