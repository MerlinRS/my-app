import React from 'react'
import AddNoteForm from '../components/AddNoteForm';
import Note from '../components/Note';
import {createContext,useState} from 'react'
import NoteData from '../data/NoteData'

const NoteContext = createContext()

function Home() {
    const [note, setNote] = useState(NoteData)
    const [noteText, updateText] = useState('')
    const [edit,noteEdit] = useState({isEdit:false,id:''})
    let noteId = 0
    
    const addNotes = (newNotes) => { 
        console.log("Hi")
        newNotes.id = note.length + 1
        const newNote = [...note,newNotes]
        setNote(newNote)  
    }

    const deleteNotes = (id) => {
        if(window.confirm("Are you sure you want to delete?")){
        setNote(note.filter((item) => item.id !== id))
        }
    }

    const editNotes = (id,text) => {
        noteId = id
        updateText(text)
        noteEdit({edit:true,id:id})
    }

    const handleUpdate = (id,newNotes) => { 
        newNotes.id = id
        console.log(id)
        console.log(note.map((item) => (item.id === id ? newNotes.text : item)))
        setNote(note.map((item) => (item.id === id ? newNotes : item)))
        noteEdit({isEdit:false,id:''})
    }

    return (

        <div className="app">
            <NoteContext.Provider value={{edit,handleUpdate}}>
                <AddNoteForm handleAdd = {addNotes} editText = {noteText}/>
                <Note notes={note} handleDelete = {deleteNotes} handleEdit = {editNotes}/>
            </NoteContext.Provider>
        </div>
    )
}
export default Home;

export {NoteContext};